#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");
  
  int x = 0, i = 0, pangkat = 0, hasilpangkat = 0;
    
    cout << "x  1/x  x^2   x^3" << endl;
    cout << "-----------------------------------"<< endl;

    x = 1;
    while (x <= 10) {
        cout << setw(5) << x << setw(8) << setprecision(5) << 1.0 / x;
        pangkat = 2;
        while (pangkat <= 3) {
            hasilpangkat = 1;
            for (i = 1; i <= pangkat; i++) {
                hasilpangkat *= x;
            }
            cout << setw(8) << hasilpangkat;
            pangkat++;
        }
        cout << endl;
        x++;
    }

  return 0;
}

